#!/usr/bin/env python
"""
For more info on how to use this file, call with -h or --help flag

Calculate various statistical parameters
(confusion matrix, precision, accuracy, recall, ...).
Expects a json file in the following format:

[
  { "filename": <String>,
    "ground truth": <String{"clean", "trackers"}>,
    "prediction": { "trackers": <Float[0,1]>, "clean": <Float[0,1]> }
  },
  ...
]

Example:

[
  { "filename": "tests/clean/in.json",
    "ground truth": "clean",
    "prediction": { "trackers": 0.3333, "clean": 0.6666 }
  },
  { "filename": "tests/trackers/1c763d1a1d94cd9a242425f515559aae52576d5e08ab069dfe82677cb15ddfba.json",
    "ground truth": "trackers",
    "prediction": { "trackers": 0.2235, "clean": 0.7865 }
  }
]


versions used:
Python             3.8.3
scikit-learn       0.22.2.post1
tabulate           0.8.7
"""

import argparse
import json
from pathlib import Path
from typing import Dict, List, Tuple, Union
from tabulate import tabulate
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score, f1_score


def parse_command_line_args() -> argparse.Namespace:
    """parse command line arguments
    for more info call this script with the -h or --help flag"""
    parser = argparse.ArgumentParser(description="calculate basic statistics for your ML model")
    parser.add_argument("-f", "--file",
                        dest="json_file",
                        metavar="<file.json>",
                        required=True,
                        help="json file containing a list of apks with predictions: REQUIRED!")
    parser.add_argument("-v", "--verbose",
                        dest="verbose",
                        type=bool,
                        metavar="<bool>",
                        help="{true, false} => print additional information when parsing an apk fails: OPTIONAL!")
    return parser.parse_args()


def get_ground_truth_and_predictions(data: List[Dict[str, Union[str, Dict[str, float]]]]) -> Tuple[List[str], List[str], int]:
    """data is input from json file, see SCHEMA.json, or description at top of this file (get-statistics.py)

    return type: ([str], [str], int)
    which represents ([ground-truths], [predictions], unparsable)
    e.g. (["trackers", "clean", "clean"],     # ground-truths
          ["trackers", "trackers", "clean"],  # predictions
          2)                                  # unparsable, meaning the nr of inputs from the json file that raised a TypeError

    # NOTE: if the verbose flag (-v, --verbose) is given then this function will print out information about unparsable data!
    NOTE: if the verbose flag (-v, --verbose) is set to true
    this function expects a global variable called parsing_errors to exist (of type list)
    if verbose, then when a TypeError occurs during parsing the error will be added to the parsing_errors list in order to be printed at the end      
    """
    ground_truths = []
    predictions = []

    unparsable = 0

    for i, apk in enumerate(data):  # type: Tuple[int, Dict]
        try:
            clean = apk["prediction"]["clean"]
            trackers = apk["prediction"]["trackers"]
            ground_truth = apk["ground truth"]

            if clean > trackers:
                predictions.append("clean")
            else:
                predictions.append("trackers")
            ground_truths.append(ground_truth)
        except TypeError:
            unparsable += 1
            if VERBOSE:
                try:
                    parsing_errors.append(f"problem with apk[{i}]: {apk}")
                except NameError:
                    continue
    return (ground_truths, predictions, unparsable)


class SizeMismatchError(Exception):
    pass


class LabelMismatchError(Exception):
    pass


if __name__ == "__main__":
    # parsing_errors will collect all TypeErrors during parsing of json file and display if verbose option (-v, --verbose) was set to true
    parsing_errors = []

    # parse command line arguments
    args = parse_command_line_args()

    VERBOSE = args.verbose
    INPUT_JSON = args.json_file
    LABELS = ["clean", "trackers"]

    # reading json
    with open(INPUT_JSON, "r") as in_file:
        unprocessed_data = json.load(in_file)

    # sanity check: keys in json file should match our expected labels
    found_keys = sorted(unprocessed_data[0]["prediction"].keys())
    if found_keys != LABELS:
        raise LabelMismatchError(f"expected {LABELS}, got {found_keys}")

    # parsing json file
    ground_truths, predictions, unparsable = get_ground_truth_and_predictions(unprocessed_data)

    # sanity checks: to calculate confusion matrix, accuracy etc each ground-truth must have a matching prediction
    if len(ground_truths) != len(predictions):
        raise SizeMismatchError(f"there should be one prediction for every ground truth, instead got size ground truth: {len(y_true)}, size predictions: {len(y_pred)}")

    # calculating confusion matrix
    cmatrix = confusion_matrix(y_true=ground_truths, y_pred=predictions, labels=LABELS)

    # calculating relevant statistics
    accuracy = accuracy_score(y_true=ground_truths, y_pred=predictions)
    precision = precision_score(y_true=ground_truths, y_pred=predictions, average=None, labels=LABELS)
    recall = recall_score(y_true=ground_truths, y_pred=predictions, average=None, labels=LABELS)
    f1 = f1_score(y_true=ground_truths, y_pred=predictions, average=None, labels=LABELS)

    # modified confusion matrix with labeled rows and columns
    cm = []
    cm.append([LABELS[0]] + cmatrix[0].tolist())
    cm.append([LABELS[1]] + cmatrix[1].tolist())

    print("# confusion matrix")
    print(tabulate(cm, headers=[""] + LABELS, tablefmt="github"))
    print()

    print("# accuracy")
    print(accuracy)

    print()
    print("# precision")
    print(tabulate([precision], headers=LABELS, tablefmt="github"))
    print()

    print("# recall")
    print(tabulate([recall], headers=LABELS, tablefmt="github"))
    print()

    if VERBOSE:
        for error in parsing_errors:
            print(error)
            print("=" * 80)
            print()
        if unparsable > 0:
            print(f"Could not analyze {unparsable} (out of {len(unprocessed_data)}) apks in the supplied file")
        else:
            print("every entry in the json file was succesfully analyzed")
